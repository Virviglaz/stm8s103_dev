#ifdef __cplusplus
extern "C"
{
#endif

#include "hardware.h"

#define SPI_nCS			PC4
#define SPI_MSCK		PC5
#define SPI_MOSI		PC6
#define SPI_MISO		PC7

static const struct {
	GPIO_TypeDef *gpio;
	enum gpio_pin pin;
	enum gpio_dir dir;
} board_pins[] = {
	{ SPI_nCS,	OUTPUT },
	{ SPI_MSCK,	OUTPUT },
	{ SPI_MOSI,	OUTPUT },
	{ SPI_MISO,	INPUT },
};

/* GPIO init for SPI */
static void init_gpio(void)
{
	u8 i;
	GPIOA->DDR = 0xFF;
	GPIOB->DDR = 0xFF;
	GPIOC->DDR = 0xFF;
	GPIOD->DDR = 0xFF;
	for (i = 0; i != ARRAY_SIZE(board_pins); i++)
		gpio_init(board_pins[i].gpio, board_pins[i].pin,
			board_pins[i].dir);

	gpio_set(SPI_nCS);
}

void init( void )
{
	init_gpio();
	delays_init();
}
#ifdef __cplusplus
} // extern "C"
#endif