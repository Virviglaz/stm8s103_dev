#ifndef __HW__
#define __HW__
#include "stm8s_mem.h"
#include "stm8s_spi.h"
#include "stm8s_gpio.h"
#include "stm8s_clk.h"
#include "stm8s_delay.h"

void init( void );
#endif
